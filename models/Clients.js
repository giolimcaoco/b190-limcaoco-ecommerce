const mongoose = require ("mongoose");

// clients as users
const clientSchema = new mongoose.Schema({
	fullName: {
		type: String, 
		required: [true, "First name is required"]
	}, 
	email: {
		type: String, 
		required: [true, "Email is required"]
	}, 
	password: {
		type: String,
		required: [true, "Password is required"]
	}, 
	mobileNo: {
		type: String, 
		required: [true, "Mobile number is required"]
	}, 
	isAdmin: {
		type: Boolean, 
		default: false
	},
	invoices: [
		{
			billingId: {
				type: String, 
				required: [true, `Billing ID is required`]
			},
			serviceId: {
				type: String, 
				required: [true, `Service ID is required`] 
			},
			serviceName: {
				type: String, 
				required: [true, `Service Name is required`]
			},
			amount: {
				type: Number, 
				required: [true, "Amount is required"]
			},
			billedOn: {
				type: Date, 
				default: new Date()
			}
		}
	]
})

module.exports=mongoose.model("Client", clientSchema)