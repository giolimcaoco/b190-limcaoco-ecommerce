const mongoose = require("mongoose");

// services as products
const serviceSchema = new mongoose.Schema ({
	serviceName: {
		type: String, 
		required: [true, "Product name is required"]
	}, 
	description: {
		type: String, 
		required: [true, "Description is required"]
	}, 
	price: {
		type: Number, 
		required: [true, "Price is required"]
	}, 
	isActive: {
		type: Boolean, 
		default: true
	}, 
	createdOn: {
		type: Date, 
		default: new Date()
	}
})

module.exports=mongoose.model("Service", serviceSchema)

/*
	Services List
		1. Web Design and Development
			Front-End Designer
			Back-End Developer
			UX/UI Designer
			Website Builder/WordPress
		2. Graphics Design
			Logo/Icon Designer
			Photoshop Editor/Retouching
			Graphic/Poster Designer
		3. Virtual Assistant - General Services
			Lead Generation
			Real State Data Administrator
			Marketing Strategist
			Customer Services
		4. Social Media Management
		5. Content Marketing, Writers and Copywriters
		6. Audio And Video Production
*/