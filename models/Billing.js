const mongoose = require("mongoose");

// billing as orders
const billingSchema = new mongoose.Schema ({
	price: {
		type: Number, 
		required: [true, "Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	clientId: {
		type: String,
		required: [true, "Clint ID is required"]
	},
	fullName: {
		type: String, 
		required: [false]
	},
	serviceId: {
		type: String,
		required: [true, `Service ID is required`]
	},
	serviceName: {
		type: String,
		required: [false]
	}
})


module.exports=mongoose.model("Billing", billingSchema)