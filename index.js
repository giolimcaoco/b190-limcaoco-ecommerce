const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const clientRoutes = require("./routes/clientRoutes.js")
const serviceRoutes = require("./routes/serviceRoutes.js")
const billingRoutes = require("./routes/billingRoutes.js")

const app = express();

mongoose.connect("mongodb+srv://giolimcaoco:Yx6WOVh2BqmAfb7y@wdc028-course-booking.bczl9fp.mongodb.net/B190-CAPSTONE-2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true 
	}
);

let db = mongoose.connection;					
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"));

app.use(cors()); 
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/clients", clientRoutes);
app.use("/services", serviceRoutes);
app.use("/billing", billingRoutes);

app.listen(process.env.PORT || 4000, () => (console.log(`API now online at port ${process.env.PORT || 4000}`)))