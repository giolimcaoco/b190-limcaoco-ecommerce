const Client = require ("../models/Clients.js")
const Service = require ("../models/Services.js")
const auth = require ("../auth.js")
const bcrypt = require ('bcrypt');




// create service
module.exports.addService = (reqBody, data) => {
	return Client.findById (data.clientId).then(result => {
		if (data.isAdmin == false ) {
			return "Administrative Privileges Required"
		} else {
			let newService = new Service({
 			serviceName : reqBody.serviceName,
 			description : reqBody.description,
 			price : reqBody.price
 		});
 			return newService.save().then((course, error) => {
 				if (error) {
 					return false;
 				} else {
 					return `${newService.serviceName} has been added`;
 				};
 			});
		}
	})
};

// get all services
 module.exports.getAllServices=()=>{
 	return Service.find({}).then(result => {
 		return result
 	})
 }

 // get ACTIVE services

  module.exports.activeServices=()=>{
 	return Service.find({isActive:true}).then(result => {
 		return result
 	})
 }

// retrieve a single service
module.exports.getService=(reqParams)=>{
		return Service.findById(reqParams.serviceId).then(result=>{
			return result
	})
}

// update A service

module.exports.updateService=(reqParams, reqBody)=>{
	let updatedService = {
		serviceName: reqBody.serviceName,
		description: reqBody.description,
		price: reqBody.price
	} 
		return Service.findByIdAndUpdate(reqParams.serviceId, updatedService).then((result, error)=>{
			if (error) {
				return false
			}else{
				return result
			}
	})
}

// archive A service
module.exports.archiveService = (reqParams) => {
    let updateActiveField = { isActive: false }
    return Service.findByIdAndUpdate(reqParams.serviceId, updateActiveField).then((result, error)=>{
        if(error){
            return false
        } else {
            return `Service has been archived`
        }
    })
}




		