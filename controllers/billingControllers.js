const Client = require ("../models/Clients.js");
const Service = require ("../models/Services.js");
const Billing = require ("../models/Billing.js");
const auth = require ("../auth.js");
const bcrypt = require ("bcrypt");


// billing a service


module.exports.billing = (data, reqBody) => {
	return Service.findById(data.serviceId).then(result => {
		if (result.isActive === false) {
			return `Service no longer being offered`
		} else {
			let newBill = new Billing({
				price: result.price, 
				clientId: data.clientId,
				serviceId: reqBody.serviceId,
				serviceName: result.serviceName

			})
			return Service.findByIdAndUpdate(data.clientId).then((service, error) => {
				return newBill.save().then((billing, error) => {
					if (error) {
						return false
					} else {
						return `Bill completed`
					}
				})
			})
		}
	})
}

// all billing

module.exports.allBilling=(data) => {
	return Client.findById(data.clientId).then(result => {
		if (data.isAdmin == false) {
			return `Administrative Privileges Required`
		} else {
			return Billing.find().then(result => {
				return result
			})
		}
	})
}

// view a bill
module.exports.getBill = (reqParams, userData) => {
		return Client.findById(userData.clientId).then(result=>{
			if (userData.isAdmin==false) {
				return `Administrative Privileges Required`
			} else {
				return Billing.findById(reqParams.billingId).then((result, error) =>{
					if (error) {
						return `Not found`
					} else {
						return result
					}
				})
			}
		})



		// return Billing.findById(reqParams.billingId).then((result, error) => {
 	// 		if (error) {
 	// 			return `Not found`
 	// 		} else {
 	// 			return result
 	// 		}
 	// })
}
