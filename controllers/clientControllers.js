const Client = require ("../models/Clients.js");
const Service = require ("../models/Services.js");
const Billing = require ("../models/Billing.js");
const auth = require ("../auth.js");
const bcrypt = require ("bcrypt");

//validate email
module.exports.checkEmailExists = (reqBody) => {
	return Client.find( { email: reqBody.email } ).then( result => {
		if ( result.length > 0) { 
			return true; 
		} else {
			return false 
		}
	})
}

// registration
module.exports.registerUser = (reqBody) => {
	return Client.find({email: reqBody.email}).then(result => {
		if (result.length>0) {
			return `You are already registered, please login instead`;
		} else {
			let newClient = new Client ({
				fullName: reqBody.fullName, 
				email: reqBody.email, 
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo
			})
			return newClient.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		}
	})
}

// client login/admin
module.exports.loginUser = ( reqBody ) => {
	return Client.findOne( { email: reqBody.email } ).then(result =>{
		if(result === null){
			return `Login credentials does not exist, please register`;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return `Error: Invalid credentials, please try again`;
			}
		}
	})
} 

// get details

module.exports.getProfile = (Info) => {
	return Client.findById (Info.clientId).then(result => {
		result.password = "";
		return result
	})
}

// set a client/user as admin

module.exports.setAsAdmin = (reqBody, userData) => {
    return Client.findById(userData.clientId).then(result => {
        if (userData.isAdmin==false) {
            return "Administrative Privileges Required"
        } else {
            let adminUpdate = {
            		isAdmin: reqBody.isAdmin
            	} 
            		return Client.findByIdAndUpdate(reqBody.clientId, adminUpdate).then((result, error)=>{
            			if (error) {
            				return false
            			}else{
            				if (result!==null) {
            					return true
            				}else{
            					return 'User not found, please have the user/client register'
            				}
            			}
            	})
            }
    });    
}

// // update client invoices
module.exports.invoice = async (data, reqBody) => {
	let updateInvoice = await Client.findById(data.clientId).then(client => {
		if (data.clientId!==null) {
			return Billing.findById(reqBody.billingId).then(result =>{
				if (result!==null){
					client.invoices.push({
						billingId: data.billingId, 
						serviceId: result.serviceId,
						serviceName: result.serviceName,
						amount: result.price
					})
					return client.save().then((client, error) => {
						if (error) {
							return false
						} else {
							return `true`
						}
					})
				} else {
					return `Billing ID does not exist`
				}
			})
		} else {
			return `Please login to update`
		}
	})
	if (updateInvoice) {
		return `Invoice has been updated`
	} else {
		return false
	}
}


// view client orders/history
module.exports.viewBills =(userData)=> {
	return Client.findById(userData.id, {invoices: 1})
}
