const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const billController = require("../controllers/billingControllers.js")


// billing/create order
router.post("/newBill", auth.verify, (req, res) => {
	//const userData = auth.decode(req.headers.authorization)
	let data = {
		clientId: auth.decode(req.headers.authorization).id, //userData.id,
		serviceId: req.body.serviceId,
	}
	billController.billing(data, req.body).then(resultFromController => res.send (resultFromController))
})

// all bills (admin)
router.get("/allBilling", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	billController.allBilling({clientid: userData.clientId, isAdmin: userData.isAdmin}).then(resultFromController => res.send (resultFromController))
})

// view a bill (admin)
router.get("/:billingId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	billController.getBill (req.params, userData).then( resultFromController => res.send (resultFromController))
})


module.exports = router;