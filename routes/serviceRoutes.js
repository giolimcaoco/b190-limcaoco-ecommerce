const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const serviceController = require("../controllers/serviceControllers.js")

// create a service

router.post("/", auth.verify, (req, res) => {
	const data = auth.decode (req.headers.authorization)
	serviceController.addService(req.body, {clientId: data.id, isAdmin: data.isAdmin}).then(resultFromController => res.send(resultFromController));
})

// get all services
router.get("/allServices", (req,res)=>{
	serviceController.getAllServices().then(resultFromController => res.send(resultFromController))
})

// get ACTIVE services
router.get("/activeServices", (req,res)=>{
	serviceController.activeServices().then(resultFromController => res.send(resultFromController))
})

// retrieve a single service
router.get("/:serviceId", (req,res)=>{
	serviceController.getService(req.params).then(resultFromController =>res.send(resultFromController))
})

// update A service
router.put("/:serviceId", auth.verify, (req,res)=>{
	const data = auth.decode (req.headers.authorization)
	serviceController.updateService(req.params,req.body,{clientId: data.id, isAdmin: data.isAdmin}).then(resultFromController =>res.send(resultFromController))
})

// archive A service

router.put("/:serviceId/archive", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
			res.send(false);
		} else {
			serviceController.archiveService(req.params).then(resultFromController => res.send (resultFromController))
		}
	})

	



module.exports = router;