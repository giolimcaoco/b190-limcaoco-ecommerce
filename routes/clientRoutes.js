const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const clientController = require("../controllers/clientControllers.js")


// checking if client exist in DB
router.post("/checkEmail", (req, res) => {
	clientController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// client registration
router.post("/register", (req, res) => {
	clientController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// client authentication
router.post("/login", ( req, res ) => {
	clientController.loginUser(req.body).then(resultFromController => res.send( resultFromController ));
} );

// get details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	clientController.getProfile ({clientId: userData.id}).then( resultFromController => res.send (resultFromController))
})

// set a client/user as admin
router.put("/:cliendId/setAsAdmin", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	clientController.setAsAdmin(req.body, {userData: userData.id, isAdmin:userData.isAdmin}).then(resultFromController=>res.send(resultFromController));
})

// update client invoices
router.post("/invoice", auth.verify, (req, res) => {
	let data = {
		clientId: auth.decode(req.headers.authorization).id,
		billingId: req.body.billingId/*,
		serviceId: req.body.serviceId*/
	}
	//const userData = auth.decode (req.headers.authorization)
	clientController.invoice(data, req.body).then(resultFromController => res.send (resultFromController))
})


// view client orders/history
router.get("/myBills", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === false){
		clientController.viewBills(userData).then(resultFromController => res.send(resultFromController));
	}
	else{
		return false
	}
});






module.exports = router;